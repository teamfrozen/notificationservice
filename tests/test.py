import unittest
from src.processdata import ProcessDataHandler as ph

class TestProcessData(unittest.TestCase):

    def test_process_text(self):
        """Test for ProcessDataHandler.process_test(str) -> list"""
        # The only correct format
        self.assertEqual(ph.process_text('Milk,2021-12-15,1'),
            ['Milk', '2021-12-15T15:00:00.000Z', '1'])

    def test_process_dictionary(self):
        """Test for ProcessDataHandler.process_dict(dict) -> str"""
        # The only correct format
        self.assertEqual(ph.process_dict(
            [
                {
                    "productName":"Milk",
                    "expirationDate":"2021-12-14T21:24:55.000Z",
                    "quantity":1
                },
                {
                    "productName":"Yogurt",
                    "expirationDate":"2021-12-14T21:24:55.000Z",
                    "quantity":1
                },
                {
                    "productName":"Cheese",
                    "expirationDate":"2021-12-14T21:24:55.000Z",
                    "quantity":1
                },
                {
                    "productName":"Fish",
                    "expirationDate":"2021-12-14T21:24:55",
                    "quantity":1
                },
                {
                    "productName":"Canned soup",
                    "expirationDate":"2021-12-14T21:24:55.000Z",
                    "quantity":1
                }
            ]),
            "\n📌 Milk: 1 till 2021\-12\-14\n📌 Yogurt: 1 till 2021\\-12\\-14\n📌 Cheese: 1 till 2021\-12\-14\n📌 Fish: 1 till 2021\-12\-14\n📌 Canned soup: 1 till 2021\-12\-14\n"
        )
            
if __name__ == '__main__':
    unittest.main()