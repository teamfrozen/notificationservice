FROM python:3.9

ADD ./src/*.py /
ADD requirements.txt /
RUN apt-get update && \
    apt-get install -y build-essential libzbar-dev libzbar0
RUN pip install --no-cache-dir --upgrade -r requirements.txt

ENTRYPOINT [ "python", "service.py"]
