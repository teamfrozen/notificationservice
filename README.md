# Notification Service [![coverage report](https://gitlab.com/teamfrozen/notificationservice/badges/main/coverage.svg)](https://gitlab.com/teamfrozen/notificationservice/-/commits/main)

This service recieves a REST message with with list of products and purpose of reminder and sends notification via Telegram.

## Bot
Link to telegram bot: t.me/SmartFridgeNotifierBot.

## Usage
This service does NOT provide external API. It interacts with [FridgeSerivce](https://gitlab.com/teamfrozen/fridgeservice) and [UsersService](https://gitlab.com/teamfrozen/UsersService).
## POC
![poc](./poc.jpg)
