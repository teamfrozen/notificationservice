from fridgeservice import FridgeServiceHandler as fh

import random, uuid, time, sys

random_food = ['Milk', 'Cheese', 'Eggs', 'Cream cheese', 'Cucumbers',
              'Tomatoes', 'Soda', 'Peant butter', 'Strawberry Jelly',
              'Bluberry Jelly', 'Bread', 'Butter', 'Cheetos', 'Lays',
              'Milky Way', 'CosmoStars', 'Pizza', 'Chicken', 'Fish', 
              'Meat', 'Grapes', 'Pantry', 'Ceral', 'Canned soup',
              'Canned beans', 'Pasta', 'Garlic', 'Bannanas', 'Avacadoes', 
              'Ramen', 'Cooking spray', 'Yogurt']

def fill_fridge_service_with_users(n, user_id=None) -> None:
    if not user_id and n == 1:
        user_id = uuid.uuid4()

    for i in range (0,n):
        if n > 1:
            user_id = uuid.uuid4()
        fh.add_user(user_id)

def clean_fridge_service(all_users) -> None:
    for user in all_users['user_id']:
        fh.delete_user(user)

def str_time_prop(start, end, time_format, prop):
    """Get a time at a proportion of a range of two formatted times.

    start and end should be strings specifying times formatted in the
    given format (strftime-style), giving an interval [start, end].
    prop specifies how a proportion of the interval to be taken after
    start.  The returned time will be in the specified format.
    """

    stime = time.mktime(time.strptime(start, time_format))
    etime = time.mktime(time.strptime(end, time_format))

    ptime = stime + prop * (etime - stime)

    return time.strftime(time_format, time.localtime(ptime))


def random_date(start, end, prop):
    return str_time_prop(start, end, '%Y-%m-%dT%H:%M:%S', prop)

def fill_fridge(user_id):
    number_of_products = random.randrange(1,10)
    meals = []
    for i in range(1, number_of_products):
        # To generate list of non-repeating meals
        meal = random.choice(random_food)
        while meal in meals:
            meal = random.choice(random_food)
        meals.append(meal)

    for meal in meals:
        date = random_date("2021-12-06T19:24:55", "2021-12-15T19:24:55", random.random()) + ".123Z"
        fh.add_product_for_user(user_id, meal, date)

if __name__ == "__main__":
    # fill_fridge_service_with_users(1)
    all_fridge_service_users = fh.get_users()
    print(all_fridge_service_users)
    # for fridgeUser in all_fridge_service_users:
    #     fill_fridge(fridgeUser['userId'])

    # clean_fridge_service(all_fridge_service_users)

    fill_fridge("1f7cdbb8-be65-44bb-a2c9-5c56033ce145")
    if len(sys.argv) > 1 and sys.argv[1] == 'with_myself':
        print('Add real user id')
        # fill_fridge_service_with_users(1, "1f7cdbb8-be65-44bb-a2c9-5c56033ce145")
    