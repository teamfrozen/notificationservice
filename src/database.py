import sqlite3

CREATE_TABLE_STMT = """ CREATE TABLE IF NOT EXISTS users (
                        chat_id text PRIMARY KEY,
                        user_id text NOT NULL
                    );
                    """

SELECT_USER_ID_STMT = """ SELECT user_id FROM users
                                WHERE chat_id='{}';
                      """

SELECT_ALL_CHAT_IDS_STMT = """ SELECT chat_id FROM users;"""

INSERT_USER_STMT = """ INSERT INTO users (chat_id, user_id)
                                          VALUES("{}","{}");
                   """

class DatabaseHandler:  
    """Handler for interaction with Telegram bot database.
    It stores Telegram chat_id and correspondig user_id from
    Users Service.
    """
                   
    def __init__(self, name) -> None:
        """ Create a database connection to SQLite database."""
        self.__connection = None
        self.__cursor = None

        if name:
            self.open(name)
    
    def __create__(self):
        """ Create table."""
        try:
            self.__cursor.execute(CREATE_TABLE_STMT)
            self.__connection.commit()
        except sqlite3.Error as error:
            print("Error creating  table: {}".format(error))

    def open(self, name):
        """ Open database."""
        try:
            self.__connection = sqlite3.connect(name, check_same_thread=False)
            self.__connection.row_factory = lambda cursor, row: row[0]
            self.__cursor = self.__connection.cursor()
            self.__create__()
        except sqlite3.Error as error:
            print("Error opening database '{}': {}".format(name, error))

    def close(self):
        """Close database."""
        if self.__connection:
            self.__connection.commit()
            self.__cursor.close()
            self.__connection.close()

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.close()

    def get_user_id(self, chat_id) -> str:
        """Get user by chat id.

           Args:
                chat_id: string
           Returns:
                user_id or None if chat_id not found.
        """
        try:
            self.__cursor.execute(SELECT_USER_ID_STMT.format(chat_id))
            user_id = self.__cursor.fetchone()
            return user_id
        except sqlite3.Error as se:
            print("DatabaseHandler.get_user_id: ", se)

    def get_chats(self) -> list[str]:
        """Get all chat ID's.
            
           Returns:
                   list of Telegram chat ID's
        """
        try:
            self.__cursor.execute(SELECT_ALL_CHAT_IDS_STMT)
            chats = self.__cursor.fetchmany()
            return chats
        except sqlite3.Error as se:
            print("DatabaseHandler.get_chats: ", se)

    def add_new_user(self, chat_id, user_id) -> None:
        """Add chat_id and user_id to table.
        
           Args:
                chat_id: string
                user_id: string
        """
        try:
            command = INSERT_USER_STMT.format(chat_id, user_id)
            self.__cursor.execute(command)
            self.__connection.commit()
        except sqlite3.Error as se:
            print("DatabaseHandler.add_new_user: ", se)
