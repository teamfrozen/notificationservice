from database import DatabaseHandler as dh
from userservice import UserServiceHandler as uh
from fridgeservice import FridgeServiceHandler as fh
from notificationservice import NotificationServiceHandler as nh
from processdata import ProcessDataHandler as ph
from dotenv import load_dotenv

from telegram import Update, ReplyKeyboardRemove
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters, CallbackContext, ConversationHandler, dispatcher
import logging, os

# Enable logging
logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO
)
logger = logging.getLogger(__name__)

# Create database
db = dh('chats.db')

# Define constants
MANUALLY, QR = range(2)

def send_message(update: Update, context: CallbackContext, message: str) -> None:
    """Send message"""
    update.message.reply_markdown_v2(message, reply_markup=ReplyKeyboardRemove())

def start(update: Update, context: CallbackContext) -> None:
    """Send a message when the command /start is issued."""
    user = update.effective_user
    chat_id = update.message.chat_id
    # Need to check if user not spamming /start
    if db.get_user_id(chat_id):
        send_message(update, context, fr'Dear SmartFridge user, you have already registered, congrats\!')
        return

    # Check if user regestered in SmartFridge
    telegram_user_id = update.message.from_user["id"]
    user_id = uh.check_telegram_id(telegram_user_id)
    # Temp crutch
    # user_id = "a4005ed7-fa65-46aa-bb25-28fc1f7ed447"
    print("User ID:",  user_id)
    if not user_id:
        send_message(update, context, fr'Seems that you are not registered yet\.\.\. \n Go to brave\-beaver\-a053ca\.netlify\.app to fix this\!')
        return

    print(chat_id, user_id)
    db.add_new_user(chat_id, user_id)

    send_message(update, context, fr'Hi {user.mention_markdown_v2()}\!\n Glad to see you as SmartFridge user\!')

def fridge(update: Update, context: CallbackContext) -> None:
    """Send a fridge contents when the command /fridge is issued."""
    chat_id = update.message.chat_id
    user_id = db.get_user_id(chat_id)

    
    fridge = fh.get_fridge_contents(user_id)
    if not bool(fridge):
        send_message(update, context, fr'Seems that your fridge is empty for now\.\.\.')
        return

    products_in_fridge = ph.process_dict(fridge)

    send_message(update, context, fr'Currently in your fridge are:' + products_in_fridge)

def process_text(update: Update, context: CallbackContext):
    """Process a text."""

    # Put results to the fridge
    chat_id = update.message.chat_id
    user_id = db.get_user_id(chat_id)
    
    products_str = update.message.text
    reply = ""
    products_list = ph.process_text(products_str)
    print(products_list)
    if not products_list:
        send_message(update, context, fr'Oops, a error occuring during processing your input\.\.\.\n'\
            'Sorry, product wasn\'t added :\(')
        return

    fh.add_product_for_user(user_id, products_list[0], products_list[1], products_list[2])

    send_message(update, context, fr'{products_list[0]} was added\!')
    return ConversationHandler.END

def process_image(update: Update, context: CallbackContext):
    """Process an image."""

    image = update.message.photo[-1].get_file()
    products = ph.process_image(image.download_as_bytearray())
    if not products:
        send_message(update, context, fr'Sorry, failed to read QR code, seems it has wrong format\.\.\.')
        return ConversationHandler.END

    # Put results to the fridge
    chat_id = update.message.chat_id
    user_id = db.get_user_id(chat_id)

    for product in products:
        fh.add_product_for_user(user_id, product['productName'], product['expirationDate'], product['quantity'])

    products_str = ph.process_dict(products)
    send_message(update, context, fr'The following products where added:{products_str}')

    return ConversationHandler.END

def add_one(update: Update, context: CallbackContext) -> None:
    """Add single product to the fridge"""

    send_message(update, context, fr'Please send a message in a format \'Product,Expiration date,Quantity\' without spaces after commas\.\nExample: \'Milk,2021\-02\-06,1\'\.\nSend /cancel to abort adding\.')
 
    return MANUALLY

def add_from_qr(update: Update, context: CallbackContext) -> None:
    """Add list of products from QR code to the fridge"""

    send_message(update, context, fr'Please send a photo containing QR with list of products\.\nSend /cancel to abort adding\.')
 
    return QR

def cancel(update: Update, context: CallbackContext):
    """Cancel conversation."""
    send_message(update, context, fr'You\'ve canceled action\.')
    return ConversationHandler.END

def notify(context: CallbackContext) -> None:
    """Check expired products in fridge and notify user."""
    total_expired = nh.check_expired()
    for user in total_expired:
        dispatcher.bot.send_message(chat_id=user['chat_id'], text="Dear SmartFridge User! Seems, that some products in your fridge expired:\n" + "\n * ".join(user['expired']))
    
    absent_favorites = nh.check_favorite()
    for user in absent_favorites:
        dispatcher.bot.send_message(chat_id=user['chat_id'], text="Dear SmartFridge User! Seems, that some products favorite products are currently missing in your fridge:\n" + "\n * ".join(user['absent']))

if __name__ == '__main__':
    load_dotenv('.env')
    updater = Updater(os.getenv('TOKEN'))

    dispatcher = updater.dispatcher

    conv_handler = ConversationHandler(
        entry_points=[CommandHandler('add_one', add_one), CommandHandler('add_from_qr', add_from_qr)],
        states={
             MANUALLY: [MessageHandler(Filters.regex('^[a-zA-Z]+,20\d\d-\d\d-\d\d,[0-9]+$'), process_text)],
             QR: [MessageHandler(Filters.photo, process_image)],
        },
        fallbacks=[CommandHandler('cancel', cancel)],
    )

    dispatcher.add_handler(CommandHandler("start", start))
    dispatcher.add_handler(CommandHandler("fridge", fridge))
    dispatcher.add_handler(conv_handler)
    # Below just for demo
    dispatcher.job_queue.run_repeating(notify, 300)

    updater.start_polling()
    updater.idle()
