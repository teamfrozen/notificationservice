from json.decoder import JSONDecodeError
import emoji
from emoji.core import emojize
from pyzbar import pyzbar
from PIL import Image
import io, json, emoji

class ProcessDataHandler:
    """Handler for processing text and image from user input."""

    @staticmethod
    def process_text(text: str) -> list:
        """Process text message of format `proudct,%YYYY-%m-%d,[0-9]`
        to the acceptable by Fridge Service dictionary.
        """
        result = {}
        try:
            splitted_text = text.split(',')
            splitted_text[1] += "T15:00:00.000Z"
            if len(splitted_text) != 3:
                raise "Invalid data in string."
            result = splitted_text
        except Exception as ex:
            print("The following error occured during text processing: ", ex)

        return result

    @staticmethod
    def process_image(image: bytearray):
        """Process a QR image with product list to the acceptable by
        Fridge Service dictionary.
        """
        result = []
        try:
            decoded_qr = pyzbar.decode(Image.open(io.BytesIO(image)))
            if not decoded_qr:
                print("Failed to retrive QR.")
                return result
            str_data = decoded_qr[0].data.decode()
            result = json.loads(str_data)
        except (pyzbar.PyZbarError, JSONDecodeError) as er:
            print("The following error occured during image processing: ", er)

        return result

    @staticmethod
    def process_dict(products: dict, target: str = 'default') -> str:
        """Process a dictionary with inputs for appending to a message."""
        products_str = "\n"
        counter = 0
        product_emoji = emoji.emojize(':pushpin:')
        print(products)
        for product in products:
            prodcut_name = product['productName']
            product_quantity = product['quantity']
            product_measure = ""
            if 'measureName' in products:
                product_measure = product['measureName']
            product_expiration = product['expirationDate'][:10]
            product_expiration = product_expiration[:4] + '\\' + product_expiration[4:7] + '\\' + product_expiration[7:]

            if target == 'expired':
                product_emoji = emoji.emojize(':x:')
            elif target == 'absent':
                product_emoji = emoji.emojize(':handbag:')
            elif target == 'close':
                product_emoji = emoji.emojize(':date:')
            
            products_str += product_emoji + " " + prodcut_name + ": " + str(product_quantity)
            if product_measure:
                products_str +=  " " + product_measure
            products_str += " till " + product_expiration + "\n"
            counter += 1
        return products_str