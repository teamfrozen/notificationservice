from database import DatabaseHandler as dh
from fridgeservice import FridgeServiceHandler as fh

from datetime import date
import time

class NotificationServiceHandler:
    """Handler for managing Fridge service and forming
    notification message.
    """
    def _compare_dates(expiration_date) -> int:
        """Return difference between dates."""
        today = date.today()
        expiration = date.fromisoformat(expiration_date)
        delta = expiration - today
        return delta.days

    def _extract_expired(products) -> list[str]:
        """Extract expired products from list."""
        expired_products = []

        for product in products:
            delta = NotificationServiceHandler._compare_dates(
                product['expirationDate'].split('T')[0]
            )
            if delta <= 0:
                expired_products.append(product['productName'])

        return expired_products

    def _extract_favorites(products) -> list[str]:
        """Extract absent favrite products from list"""
        favorite_products = []

        for product in products:
            if product['isFavorite'] and product['quantity'] == 0:
                favorite_products.append(product['productName'])
        return favorite_products

    @staticmethod
    def check_expired() -> list[dict]:
        """Checks all users in Telegram database, gets
        fridge contents per every user from Fridge Service,
        validates every product expiration date.

           Returns:
                   list of dictionaries in format:
                   [
                      {
                          'chat_id': Telegran chat_id,
                          'expired': [list of expired prodcuts]
                      }
                   ]
        """
        total_expired = []

        db = dh('chats.db')
        chats = db.get_chats();

        for chat in chats:
            user = db.get_user_id(chat)
            products = fh.get_fridge_contents(user)
            expired_products = NotificationServiceHandler._extract_expired(products) 
            total_expired.append({
                'chat_id': chat,
                'expired': expired_products           
            })

        return total_expired

    @staticmethod
    def check_favorite() -> list[dict]:
        """Checks favorite prodcuts absence in Fridge Serivce

           Returns: list of favorite products, which quantity
           is currently 0
        """
        absent_favorite = []
        db = dh('chats.db')
        chats = db.get_chats();

        for chat in chats:
            user = db.get_user_id(chat)
            products = fh.get_fridge_contents(user)
            expired_products = NotificationServiceHandler._extract_favorites(products) 
            absent_favorite.append({
                'chat_id': chat,
                'absent': expired_products
            })

        return absent_favorite
