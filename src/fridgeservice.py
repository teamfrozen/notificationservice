import requests, json, os

class FridgeServiceHandler:
    """Handler for interaction with Fridge Service."""

    BASE_URL = os.getenv('FRIDGE_SERVICE_URL')

    @staticmethod
    def add_user(user_id, fridge_id = None) -> None:
        """Add a new user with `user_id` to the Fridge Service.

           Args:
                user_id: string in UUID format.
                fridge_id: string in UUID format. If None, Fridge Service will
                create new fridge_id automatically.
        """
        try:
            add_user_req = FridgeServiceHandler.BASE_URL + "/api/Users"
            response = requests.post(add_user_req, json = {
                'userId': user_id,
                'fridgeId': fridge_id
            })
            response.raise_for_status()
        except requests.RequestException as re:
            print("FridgeServiceHandler.add_user:", re.response)

    @staticmethod
    def get_users() -> list[dict]:
        """Return list of all avaliable in the Fridge Service users with
           their fridges.

           Returns:
                   [{user_id,fridge_id}, ...]
        """
        try:
            get_users_req = FridgeServiceHandler.BASE_URL + "/api/Users"
            response = requests.get(get_users_req)
            response.raise_for_status()
            return json.loads(response.text)
        except requests.RequestException as re:
            print("FridgeServiceHandler.get_users:", re.response)

    @staticmethod
    def delete_user(user_id) -> None:
        """Delete user with user_id from the FridgeService.

           Args:
                user_id: string in UUID format.
        """
        try:
            del_user_req = FridgeServiceHandler.BASE_URL + "/api/Users"
            response = requests.delete(del_user_req, params=user_id)
            response.raise_for_status()
        except requests.RequestException as re:
            print("FridgeServiceHandler.delete_user:", re.response)

    @staticmethod
    def add_product_for_user(user_id, product, expiration_date, quantity=1) -> None:
        """Add new product into the FridgeService.

           Args:
                user_id: string in UUID format.
                product: string with name of a product
                expiration_date: string in '%Y-%m-%dT%H:%M:%S.%f' format
                quantity: integer, optinal parameter
        """
        try:
            add_product_req = FridgeServiceHandler.BASE_URL + "/api/Fridges/OwnerContents/" + user_id + "/AddProductManually"
            response = requests.post(add_product_req, json = {
                'productName': product,
                'expirationDate': expiration_date,
                'quantity': quantity
            })
            response.raise_for_status()
        except requests.RequestException as re:
            print("FridgeServiceHandler.add_product_for_user:", re.response)

    @staticmethod
    def get_fridge_contents(user_id) -> list[dict]:
        """Get contents of fridge for user_id.

           Args:
                user_id: string in UUID format.
        """
        try:
            get_fridge_contents = FridgeServiceHandler.BASE_URL + "/api/Fridges/OwnerContents/" + user_id
            response = requests.get(get_fridge_contents)
            response.raise_for_status()
            return json.loads(response.text)
        except requests.RequestException as re:
            print("FridgeServiceHandler.get_fridge_contents:", re.response)

