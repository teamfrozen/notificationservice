import requests, json, os

class UserServiceHandler:
    """Handler for interaction with Users Service."""

    BASE_URL = os.getenv('USERS_SERVICE_URL')

    @staticmethod
    def check_telegram_id(telegram_user_id) -> str:
        """Check if Telegram user provided access to Telegram
        during registration in Users Service.

           Args:
                telegram_user_id: string
        
           Returns:
                True if Telegram user provided Telegram access 
                during registration, Else otherwise.
        """
        try:
            check_user_url = UserServiceHandler.BASE_URL + "/api/v1/users/telegram/" + str(telegram_user_id)
            response = requests.get(check_user_url, headers={'apiKey' : 'TODO'})
            print(response.text)
            if response.status_code == 401:
                return str()
            elif not response.ok:
                response.raise_for_status()
            else:
                responce_dict = json.loads(response.text)
                return responce_dict['payload']['id']
        except requests.RequestException as re:
            print("UsersServiceHandler.check_telegram_id:", re.strerror)
    